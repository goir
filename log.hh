#ifndef _GOIR_LOG_HH
#define _GOIR_LOG_HH

#include <cstdarg>
#include <cstdio>

namespace goir {
namespace log {

  typedef enum {
    wm_trace, wm_summary, // watermarks
    histo,
    data_out,
    edge_details,
    debug_eval,

    NULLFLAG = -1
  } flag;

  /* Tracing flags currently in effect. */
  extern unsigned flags;

  /*
   * Takes a NULLFLAG-terminated list of flag arguments, and return
   * a bitmask with all those flags set.
   */
  static inline unsigned bitmask(flag f, ...) {
    unsigned mask = 1 << f;
    va_list vl;
    va_start(vl, f);
    do {
      flag vf = (flag)va_arg(vl, int);
      if (vf == NULLFLAG) break;
      mask |= 1 << vf;
    } while(1);
    va_end(vl);

    return mask;
  }

  static inline bool is_set(flag f) { return (flags & bitmask(f, NULLFLAG)); }

  /*
   * Trace to stderr, conditionned by the activation of the given
   * flag.
   */
  static inline void trace(flag f, const char* fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    if (is_set(f))
      vfprintf(stderr, fmt, vl);
    va_end(vl);
  }

}
}

#endif // _GOIR_LOG_HH

#
# WARNING
#
# This makefile is only provided for convenience, for people who don't
# have jam.  It only supports building from scratch, dependencies are
# not tracked - use jam for that.
#

default:
	@echo 'Well, you meant "jam", right ?'
	@false

CXXFLAGS=-W -Wall -ggdb3 -O1 -fno-inline
LIBS=-ljpeg -lpng

line: line.o ImageInfo.o sImLib/Image.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LIBS)

clean:
	rm -f *.o sImLib/*.o line

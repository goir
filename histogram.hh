#include <cassert>
#include <cstdio>
#include <cstring>

namespace goir {

  class Histogram {
  private:
    unsigned _steps;
    unsigned _max;
    unsigned _samples;
    unsigned *_data;
  public:
    Histogram(unsigned steps, unsigned max)
      : _steps(steps)
      , _max(max)
      , _samples(0)
      , _data(new unsigned[_steps]) {
      memset(_data, 0, _steps*sizeof(unsigned));
    }
    Histogram(const Histogram & h)
      : _steps(h._steps)
      , _max(h._max)
      , _samples(h._samples)
      , _data(new unsigned[_steps]) {
      memcpy(_data, h._data, _steps*sizeof(unsigned));
    }
    ~Histogram() {
      delete[] _data;
    }

    class ValueNotFoundException { };

    unsigned samples() { return _samples; }

    void record(unsigned value) {
      unsigned slot = value * (_steps - 1) / _max;
      assert(slot<_steps);
      _samples++;
      _data[slot] ++;
    }

    unsigned min_nonempty() const {
      for (unsigned i = 0; i<_steps; i++)
	if (_data[i] != 0) return i;
      throw new ValueNotFoundException;
    }

    static const unsigned tolerance_base = 1000000;

    unsigned max_nonempty(unsigned tolerance = 0) const {
      unsigned tolerated = tolerance * _samples / tolerance_base;
      for (int i = _steps-1; i>0; i--)
	if (_data[i] != 0) {
	  // return index of non-empty slot as soon as tolerance is over
	  if (tolerated == 0) return i;
	  // else decrease tolerance accordingly
	  if (_data[i] >= tolerated)
	    tolerated = 0;
	  else
	    tolerated -= _data[i];
	}
      throw new ValueNotFoundException;
    }

    void display() {
      unsigned low, high;
      try {
	low = min_nonempty();
	high = max_nonempty();
      } catch (ValueNotFoundException *e) {
	printf("(bounds not found!)");
	low = 0; high = _steps - 1;
      }
      printf("RANGE: %u - %u:", low, high);
      for (unsigned i = low; i <= high; i++)
	  printf(" %d", _data[i]);
      printf("\n");
    }

    void display_potential_false_matches() {
      printf("0.001: %u - 0.005: %u - 0.01: %u\n",
	     max_nonempty(1000), max_nonempty(5000), max_nonempty(10000));
    }

  };

}

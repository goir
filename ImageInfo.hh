#ifndef _GOIR_IMAGEINFO_HH
#define _GOIR_IMAGEINFO_HH

#include "sImLib/Image.hh"

#include "histogram.hh"
#include "util.hh"
#include "uvector.hh"

#include <cstdio>
#include <vector>

namespace goir {

  class ImageInfo {

    class LineData;

  public:
    enum { ALGO_1PIX_PERCOMP,
	   ALGO_1PIX_COMPSUM,
	   ALGO_1PIX_COMPSUM_FILTERED,
	   // 	 ALGO_1PIX_COMBINED,
	   ALGO_2PIX_PERCOMP,
	   ALGO_2PIX_COMPSUM,
	   // 	 ALGO_2PIX_COMBINED,

	   NB_ALGOS
    };

  private:

    class AlgoResults {
    public:
      struct MatchData {
	unsigned x,y;
	unsigned delta;
	bool false_match;

	MatchData(unsigned _x, unsigned _y, unsigned _delta, bool _is_known = true)
	  : x(_x), y(_y), delta(_delta), false_match(!_is_known) {};
	MatchData(const std::pair<unsigned,unsigned> p)
	  : x(p.first), y(p.second), delta(0), false_match(false) {};
      };

      // results of algorithm
      std::vector<MatchData> found_edges;

      /*** everything below is for analysis only ***/

      const LineData* linedata;

      // watermarks for calibration
      unsigned min_positive, max_negative;

      std::vector<MatchData> missed;

      AlgoResults(unsigned maxvalue, const LineData* _linedata)
	: linedata(_linedata)
	, min_positive(maxvalue), max_negative(0)
      {}

      void update_watermarks(bool is_edge, unsigned value);
      void fprint(FILE* outf);
      void draw_on(sImLib::Image & img);
    };

    class LineData {
    private:
      ImageInfo* ii; // line lives in this image
      bool _edges_are_known;

    public:
      unsigned x0, y0, x1, y1;
      UVector known_edges;
      std::vector<AlgoResults> results;

      LineData(ImageInfo* _ii, unsigned _x0, unsigned _y0, unsigned _x1, unsigned _y1)
	: ii(_ii)
	, _edges_are_known(false)
	, x0(_x0), y0(_y0), x1(_x1), y1(_y1)
      {
	// FIXME: I hate that way of initializing a vector...
	results.push_back(AlgoResults(255, this)); // 1PIX_PERCOMP
	results.push_back(AlgoResults(3*255, this)); // 1PIX_COMPSUM
	results.push_back(AlgoResults(3*255, this)); // 1PIX_COMPSUM_FILTER
	results.push_back(AlgoResults(255, this)); // 2PIX_PERCOMP
	results.push_back(AlgoResults(3*255, this)); // 2PIX_COMPSUM
	assert(NB_ALGOS == 5);
      }

      // Read a list of unsigned values as coords of edges along the line
      void read_edges(FILE* f);

      bool edges_are_known() const {
	return _edges_are_known;
      }

      void add_to_histo() {
	ii->add_to_histo(x0, y0, x1, y1, &known_edges);
      }

      void find_edges();
      void fprint(FILE* outf);

    private:
      void maybe_update_watermarks(int algo_id, unsigned x,
				   bool is_edge, unsigned value);
    };

  private:
    const sImLib::Image * const imgp;

    std::vector<Histogram> histos;
    std::vector< std::vector<Histogram> > histos_c;
    std::vector<Histogram> histos_allc;

  public:
    std::vector<LineData*> lines;
    enum { NONEDGES, EDGES };

    ImageInfo(const sImLib::Image* const _imgp)
      : imgp(_imgp)
      , histos(2, Histogram(3*256, 3*256))
      , histos_c (2, std::vector<Histogram>(3, Histogram(256,256)))
      , histos_allc(2, Histogram(256, 256))
    {}

    void maybe_read_edge_data();

    void add_line(unsigned x0, unsigned y0, unsigned x1, unsigned y1) {
      lines.push_back(new LineData(this, x0, y0, x1, y1));
    }

    void collect_histo_from_lines() {
      const_foreach(i, lines)
	(*i)->add_to_histo();
    }

    void display_histo();

    void look_for_edges() const {
      const_foreach(i, lines)
	(*i)->find_edges();
    }

    void fprint(FILE* outf) const {
      fprintf(outf, "# GoIR data\n");
      const_foreach(i, lines)
	(*i)->fprint(outf);
    }

  private:
    void add_to_histo(unsigned x0, unsigned y0, unsigned x1, unsigned y1,
		      UVector *known_edges = NULL);
  };

}

#endif // _GOIR_IMAGEINFO_HH

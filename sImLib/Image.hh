/*
 * This is a Simple Image Library.  It borrows some code from CImg,
 * but focusses on keeping a pixel's component adjacent (for our
 * purpose, that implies clarity of caller's code and better
 * performance), and providing macros to do any operation on paths
 * within an image, not only drawing such paths.
 */

#ifndef _SIMLIB_IMAGE_HH
#define _SIMLIB_IMAGE_HH

#include <string>
#include <cassert>

namespace sImLib {

  typedef unsigned char component;
  typedef component pixel[3];

  class Image {
  public:
    // load from jpeg file
    Image(const std::string & _filename);
    // copy
    Image(const Image& image);

    ~Image();

    // direct access to data
    unsigned width, height;
    pixel* data;
    const std::string filename;

    // address of given pixel in data
    pixel* ptr(unsigned x, unsigned y) const {
      return (data + (x + y * width));
    }

    // pixel coordinates of address within data
    unsigned x(pixel* ptrd) const {
      assert(ptrd >= data);
      unsigned offset = ptrd - data;
      assert(offset < width * height);
      return (offset % width);
    }
    unsigned y(pixel* ptrd) const {
      assert(ptrd >= data);
      unsigned offset = ptrd - data;
      assert(offset < width * height);
      return (offset / width);
    }

    // show image
    void display() const;

    // write to image file
    int write_jpeg(const std::string & filename, int quality = 100) const;
    int write_jpeg(int fd, int quality = 100) const;
    int write_png(const std::string & filename, bool quick=false) const;
    int write_png(int fd, bool quick=false) const;

    // drawing methods (mostly for testing)
    void draw_line(const int x0, const int y0, const int x1, const int y1,
		   const pixel& color, const float opacity = 1.0);
  };

  /*
   * Some useful constants...
   */
  const pixel black = {0,0,0};
  const pixel white = {255,255,255};
  const pixel grey50 = {128,128,128};
  const pixel grey75 = {64,64,64};

  const pixel red = {255,0,0};
  const pixel green = {0,255,0};
  const pixel blue = {0,0,255};
  const pixel red50 = {64,0,0};
  const pixel green50 = {0,64,0};
  const pixel blue50 = {0,0,64};

  const pixel yellow = {255,255,0};
  const pixel cyan = {0,255,255};
  const pixel magenta = {255,0,255};
}

#endif // _SIMLIB_IMAGE_HH

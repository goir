#ifndef _SIMLIB_IMGPATH_HH
#define _SIMLIB_IMGPATH_HH

#include <algorithm>
#include <cstdlib>

/*
 * Walking macros from this file are adapted from CImg drawing
 * methods.
 */

// FIXME: width cropping does not work !?

/*
 * forline:
 *
 * Run body_code over all pixels of the (x0,y0)-(x1,y1) line.
 *
 * Demo example:
 *  sImLib_forline(img, x, y, ptrd,
 *                 0, 0, img.width, img.height,
 *                 if (x%2)
 *                   *ptrd = blue[k];
 *                 else
 *                   *ptrd = red[k];
 *                 printf("%d,%d ", x, y);
 *                 );
 */
#define sImLib_forline(img,ptrd,x0,y0,x1,y1,body_code)			\
  do {									\
    int nx0 = (x0), nx1 = (x1), ny0 = (y0), ny1 = (y1);			\
  									\
    if (nx0>nx1) { std::swap(nx0,nx1); std::swap(ny0,ny1); }		\
    if (nx1<0 || nx0>=(int)(img)->width) break;				\
    if (nx0<0) { ny0-=nx0*(ny1-ny0)/(nx1-nx0); nx0=0; }			\
    if (nx1>=(int)(img)->width)						\
      { ny1+=(nx1-(img)->width)*(ny0-ny1)/(nx1-nx0); nx1=(img)->width-1;} \
    if (ny0>ny1) { std::swap(nx0,nx1); std::swap(ny0,ny1); }		\
    if (ny1<0 || ny0>=(int)(img)->height) break;			\
    if (ny0<0) { nx0-=ny0*(nx1-nx0)/(ny1-ny0); ny0=0; }			\
    if (ny1>=(int)(img)->height)					\
      { nx1+=(ny1-(img)->height)*(nx0-nx1)/(ny1-ny0); ny1=(img)->height-1;} \
  									\
    const bool steep = (ny1-ny0)>std::abs(nx1-nx0);			\
    if (steep)   { std::swap(nx0,ny0); std::swap(nx1,ny1); }		\
    if (nx0>nx1) { std::swap(nx0,nx1); std::swap(ny0,ny1); }		\
    const int								\
      dx = nx1-nx0, dy = std::abs(ny1-ny0),				\
      offx = steep?(img)->width:1,					\
      offy = (ny0<ny1?1:-1)*(steep?1:(img)->width);			\
  									\
    sImLib::pixel *ptrd = steep?(img)->ptr(ny0,nx0):(img)->ptr(nx0,ny0); \
    for (int error=0, _x=nx0, linechange=0;				\
	 _x <= nx1;							\
	 _x++, linechange=(((error+=dy)<<1)>=dx),			\
	   ptrd+=offx, ptrd+=offy*linechange, error-=dx*linechange)	\
      do { body_code; } while(0);					\
  } while(0)

#endif // _SIMLIB_IMGPATH_HH

#include "Image.hh"
#include "img_path.hh"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>

#include <fcntl.h>
#include <unistd.h>
// for random
#include <sys/time.h>

#include <png.h>
#include <jpeglib.h>
#if 0
  // libjpeg needs it, but libpng wants to do the include itself
#include <setjmp.h>
#endif

// the class' methods themselves...
namespace sImLib {

  // forward decls for implementation
  static int read_JPEG_file (const char * filename,
			     pixel** outbufferp,
			     unsigned* widthp, unsigned* heightp);
  static void write_JPEG_file (int fd, const pixel *const buffer,
			       unsigned width, unsigned height,
			       int quality);
  static int write_PNG_file(int fd, const sImLib::Image * const imgp,
			    bool quick);

  Image::Image(const std::string & _filename)
    : filename(_filename) {
    if (!read_JPEG_file(filename.c_str(), &data, &width, &height)) {
      fprintf(stderr, "cannot read jpeg file\n");
      exit(1);
    }

    // see rng for empfile creation
    struct timeval tv;
    if (gettimeofday(&tv, NULL) < 0) {
      fprintf(stderr, "cannot read time to seed rng: %s\n", strerror(errno));
      exit(1);
    }
    srandom(tv.tv_sec);
  }

  Image::Image(const Image& image)
    : width(image.width)
    , height(image.height)
    , filename(image.filename) {
    unsigned size = width * height * sizeof(pixel);
    data = (pixel*)malloc(size);
    memcpy(data, image.data, size);
  }

  Image::~Image() {
    free(data);
  }

  int Image::write_jpeg(const std::string & out_filename, int quality) const {
    int fd = open(out_filename.c_str(), O_WRONLY|O_CREAT,
		  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (fd < 0) {
      fprintf(stderr, "can't open %s: %s\n", out_filename.c_str(), strerror(errno));
      exit(1);
    }
    this->write_jpeg(fd, quality);
    return 1;					  // FIXME
  }

  int Image::write_jpeg(int fd, int quality) const {
    write_JPEG_file(fd, data, width, height, quality);
    return 1;					  // FIXME
  }

  int Image::write_png(const std::string & out_filename, bool quick) const {
    int fd = open(out_filename.c_str(), O_WRONLY|O_CREAT,
		  S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if (fd < 0) {
      fprintf(stderr, "can't open %s: %s\n", out_filename.c_str(), strerror(errno));
      exit(1);
    }
    return this->write_png(fd, quick);
  }

  int Image::write_png(int fd, bool quick) const {
    return write_PNG_file(fd, this, quick);
  }

  void Image::display() const {
    // Write to temporary image file
    const char pattern[] = "/tmp/goir-tmp%05lu.png";
    char tmpname[sizeof(pattern) + 1];
    sprintf(tmpname, pattern, random() % 100000);
    int tmpfd = open(tmpname, O_WRONLY|O_CREAT,
		     O_EXCL|S_IRUSR|S_IWUSR);
    this->write_png(tmpfd, true);

    // display using imagemagick
    const char cmdname[]="gm display";
    char cmd[sizeof(pattern) + sizeof(cmdname) + 2];
    snprintf(cmd, sizeof(cmd), "%s %s", cmdname, tmpname);
    system(cmd);

    //cleanup
    close(tmpfd);
    unlink(tmpname);
  }

  void Image::draw_line(const int x0, const int y0, const int x1, const int y1,
			const pixel& color, const float opacity) {
    assert (opacity >= 0.0 && opacity <= 1.0);
    if (opacity == 1.0)
      sImLib_forline(this, ptrd, x0, y0, x1, y1, memcpy(ptrd, &color, 3) );
    else {
      const float copacity = 1.0 - opacity;
      const float scaled_color[3] = { color[0]*opacity,
				      color[1]*opacity,
				      color[2]*opacity };
      sImLib_forline(this, ptrd, x0, y0, x1, y1,
		     for (unsigned k=0; k<3; k++)
		       (*ptrd)[k] = (component)(scaled_color[k] + copacity * *(ptrd)[k]);
		     );
    }
  }

  //////////////////////////////////////////////////
  // Code mostly stolen from libjpeg's own example.

  /*
   * Sample routine for JPEG compression.  We assume that the target file name
   * and a compression quality factor are passed in.
   */

  static void write_JPEG_file (int fd, const pixel *const buffer,
			       unsigned width, unsigned height,
			       int quality)
  {
    /* This struct contains the JPEG compression parameters and pointers to
     * working space (which is allocated as needed by the JPEG library).
     * It is possible to have several such structures, representing multiple
     * compression/decompression processes, in existence at once.  We refer
     * to any one struct (and its associated working data) as a "JPEG object".
     */
    struct jpeg_compress_struct cinfo;
    /* This struct represents a JPEG error handler.  It is declared separately
     * because applications often want to supply a specialized error handler
     * (see the second half of this file for an example).  But here we just
     * take the easy way out and use the standard error handler, which will
     * print a message on stderr and call exit() if compression fails.
     * Note that this struct must live as long as the main JPEG parameter
     * struct, to avoid dangling-pointer problems.
     */
    struct jpeg_error_mgr jerr;
    /* More stuff */
    FILE * outfile;               /* target file */
    JSAMPROW row_pointer[1];      /* pointer to JSAMPLE row[s] */

    /* Step 1: allocate and initialize JPEG compression object */

    /* We have to set up the error handler first, in case the initialization
     * step fails.  (Unlikely, but it could happen if you are out of memory.)
     * This routine fills in the contents of struct jerr, and returns jerr's
     * address which we place into the link field in cinfo.
     */
    cinfo.err = jpeg_std_error(&jerr);
    /* Now we can initialize the JPEG compression object. */
    jpeg_create_compress(&cinfo);

    /* Step 2: specify data destination (eg, a file) */
    /* Note: steps 2 and 3 can be done in either order. */

    /* Here we use the library-supplied code to send compressed data to a
     * stdio stream.  You can also write your own code to do something else.
     * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
     * requires it in order to write binary files.
     */
    if ((outfile = fdopen(fd, "wb")) == NULL) {
      fprintf(stderr, "can't fdopen %d: %s\n", fd, strerror(errno));
      exit(1);
    }
    jpeg_stdio_dest(&cinfo, outfile);

    /* Step 3: set parameters for compression */

    /* First we supply a description of the input image.
     * Four fields of the cinfo struct must be filled in:
     */
    cinfo.image_width = width;            /* image width and height, in pixels */
    cinfo.image_height = height;
    cinfo.input_components = 3;           /* # of color components per pixel */
    cinfo.in_color_space = JCS_RGB;       /* colorspace of input image */
    /* Now use the library's routine to set default compression parameters.
     * (You must set at least cinfo.in_color_space before calling this,
     * since the defaults depend on the source color space.)
     */
    jpeg_set_defaults(&cinfo);
    /* Now you can set any non-default parameters you wish to.
     * Here we just illustrate the use of quality (quantization table) scaling:
     */
    jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

    /* Step 4: Start compressor */

    /* TRUE ensures that we will write a complete interchange-JPEG file.
     * Pass TRUE unless you are very sure of what you're doing.
     */
    jpeg_start_compress(&cinfo, TRUE);

    /* Step 5: while (scan lines remain to be written) */
    /*           jpeg_write_scanlines(...); */

    /* Here we use the library's state variable cinfo.next_scanline as the
     * loop counter, so that we don't have to keep track ourselves.
     * To keep things simple, we pass one scanline per call; you can pass
     * more if you wish, though.
     */
    while (cinfo.next_scanline < cinfo.image_height) {
      /* jpeg_write_scanlines expects an array of pointers to scanlines.
       * Here the array is only one element long, but you could pass
       * more than one scanline at a time if that's more convenient.
       */
      row_pointer[0] = (JSAMPLE*) & buffer[cinfo.next_scanline * width];
      (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }

    /* Step 6: Finish compression */

    jpeg_finish_compress(&cinfo);
    /* After finish_compress, we can close the output file. */
    fclose(outfile);

    /* Step 7: release JPEG compression object */

    /* This is an important step since it will release a good deal of memory. */
    jpeg_destroy_compress(&cinfo);

    /* And we're done! */
  }


  /*
   * ERROR HANDLING:
   *
   * The JPEG library's standard error handler (jerror.c) is divided into
   * several "methods" which you can override individually.  This lets you
   * adjust the behavior without duplicating a lot of code, which you might
   * have to update with each future release.
   *
   * Our example here shows how to override the "error_exit" method so that
   * control is returned to the library's caller when a fatal error occurs,
   * rather than calling exit() as the standard error_exit method does.
   *
   * We use C's setjmp/longjmp facility to return control.  This means that the
   * routine which calls the JPEG library must first execute a setjmp() call to
   * establish the return point.  We want the replacement error_exit to do a
   * longjmp().  But we need to make the setjmp buffer accessible to the
   * error_exit routine.  To do this, we make a private extension of the
   * standard JPEG error handler object.  (If we were using C++, we'd say we
   * were making a subclass of the regular error handler.)
   *
   * Here's the extended error handler struct:
   */

  struct my_error_mgr {
    struct jpeg_error_mgr pub;    /* "public" fields */

    jmp_buf setjmp_buffer;        /* for return to caller */
  };

  typedef struct my_error_mgr * my_error_ptr;

  /*
   * Here's the routine that will replace the standard error_exit method:
   */

  METHODDEF(void)
    my_error_exit (j_common_ptr cinfo)
  {
    /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
    my_error_ptr myerr = (my_error_ptr) cinfo->err;

    /* Always display the message. */
    /* We could postpone this until after returning, if we chose. */
    (*cinfo->err->output_message) (cinfo);

    /* Return control to the setjmp point */
    longjmp(myerr->setjmp_buffer, 1);
  }


  /*
   * Sample routine for JPEG decompression.  We assume that the source file name
   * is passed in.  We want to return 1 on success, 0 on error.
   */
  static int read_JPEG_file (const char * filename, pixel** outbufferp,
			     unsigned* widthp, unsigned* heightp)
  {
    /* This struct contains the JPEG decompression parameters and pointers to
     * working space (which is allocated as needed by the JPEG library).
     */
    struct jpeg_decompress_struct cinfo;
    /* We use our private extension JPEG error handler.
     * Note that this struct must live as long as the main JPEG parameter
     * struct, to avoid dangling-pointer problems.
     */
    struct my_error_mgr jerr;
    /* More stuff */
    FILE * infile;                /* source file */
    JSAMPARRAY buffer;            /* Output row buffer */
    int row_stride;               /* physical row width in output buffer */
    pixel* buffer_lineptr;

    /* In this example we want to open the input file before doing anything else,
     * so that the setjmp() error recovery below can assume the file is open.
     * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
     * requires it in order to read binary files.
     */

    if ((infile = fopen(filename, "rb")) == NULL) {
      fprintf(stderr, "can't open %s\n", filename);
      return 0;
    }

    /* Step 1: allocate and initialize JPEG decompression object */

    /* We set up the normal JPEG error routines, then override error_exit. */
    cinfo.err = jpeg_std_error(&jerr.pub);
    jerr.pub.error_exit = my_error_exit;
    /* Establish the setjmp return context for my_error_exit to use. */
    if (setjmp(jerr.setjmp_buffer)) {
      /* If we get here, the JPEG code has signaled an error.
       * We need to clean up the JPEG object, close the input file, and return.
       */
      jpeg_destroy_decompress(&cinfo);
      fclose(infile);
      return 0;
    }
    /* Now we can initialize the JPEG decompression object. */
    jpeg_create_decompress(&cinfo);
    /* Step 2: specify data source (eg, a file) */

    jpeg_stdio_src(&cinfo, infile);

    /* Step 3: read file parameters with jpeg_read_header() */

    (void) jpeg_read_header(&cinfo, TRUE);
    /* We can ignore the return value from jpeg_read_header since
     *   (a) suspension is not possible with the stdio data source, and
     *   (b) we passed TRUE to reject a tables-only JPEG file as an error.
     * See libjpeg.doc for more info.
     */

    /* Step 4: set parameters for decompression */

    /* In this example, we don't need to change any of the defaults set by
     * jpeg_read_header(), so we do nothing here.
     */

    /* Step 5: Start decompressor */

    (void) jpeg_start_decompress(&cinfo);
    /* We can ignore the return value since suspension is not possible
     * with the stdio data source.
     */

    /* We may need to do some setup of our own at this point before reading
     * the data.  After jpeg_start_decompress() we have the correct scaled
     * output image dimensions available, as well as the output colormap
     * if we asked for color quantization.
     * In this example, we need to make an output work buffer of the right size.
     */
    *widthp = cinfo.output_width;
    *heightp = cinfo.output_height;
    assert (cinfo.output_components == 3); // FIXME: lacks support for greyscale
    assert (sizeof(pixel) == 3);
    *outbufferp = (pixel*)malloc(cinfo.output_width * cinfo.output_height *
				 sizeof(pixel));
    buffer_lineptr = *outbufferp;

    /* JSAMPLEs per row in output buffer */
    row_stride = cinfo.output_width * cinfo.output_components;
    /* Make a one-row-high sample array that will go away when done with image */
    buffer = (*cinfo.mem->alloc_sarray)
      ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);

    /* Step 6: while (scan lines remain to be read) */
    /*           jpeg_read_scanlines(...); */

    /* Here we use the library's state variable cinfo.output_scanline as the
     * loop counter, so that we don't have to keep track ourselves.
     */
    while (cinfo.output_scanline < cinfo.output_height) {
      /* jpeg_read_scanlines expects an array of pointers to scanlines.
       * Here the array is only one element long, but you could ask for
       * more than one scanline at a time if that's more convenient.
       */
      (void) jpeg_read_scanlines(&cinfo, buffer, 1);
      /* Copy into our own buffer */
      memcpy(buffer_lineptr, buffer[0], row_stride);
      buffer_lineptr += cinfo.output_width;
    }

    /* Step 7: Finish decompression */

    (void) jpeg_finish_decompress(&cinfo);
    /* We can ignore the return value since suspension is not possible
     * with the stdio data source.
     */

    /* Step 8: Release JPEG decompression object */

    /* This is an important step since it will release a good deal of memory. */
    jpeg_destroy_decompress(&cinfo);

    /* After finish_decompress, we can close the input file.
     * Here we postpone it until after no more JPEG errors are possible,
     * so as to simplify the setjmp error logic above.  (Actually, I don't
     * think that jpeg_destroy can do an error exit, but why assume anything...)
     */
    fclose(infile);

    /* At this point you may want to check to see whether any corrupt-data
     * warnings occurred (test whether jerr.pub.num_warnings is nonzero).
     */

    /* And we're done! */
    return 1;
  }


  ///////////////////////////////////////////////////////////////
  // Code mostly stolen from libpng's public-domain own example.

  /* write a png file */
  static int write_PNG_file(int fd, const sImLib::Image * const imgp,
			    bool quick)
  {
    FILE *fp;
    png_structp png_ptr;
    png_infop info_ptr;

    /* open the file */
    fp = fdopen(fd, "wb");
    if (fp == NULL)
      return (-1);

    /* Create and initialize the png_struct with the desired error handler
     * functions.  If you want to use the default stderr and longjump method,
     * you can supply NULL for the last three parameters.  We also check that
     * the library version is compatible with the one used at compile time,
     * in case we are using dynamically linked libraries.  REQUIRED.
     */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
				      png_voidp_NULL,
				      png_error_ptr_NULL,
				      png_error_ptr_NULL);

    if (png_ptr == NULL)
      {
	fclose(fp);
	return (-1);
      }

    /* Allocate/initialize the image information data.  REQUIRED */
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL)
      {
	fclose(fp);
	png_destroy_write_struct(&png_ptr,  png_infopp_NULL);
	return (-1);
      }

    /* Set error handling.  REQUIRED if you aren't supplying your own
     * error handling functions in the png_create_write_struct() call.
     */
    if (setjmp(png_jmpbuf(png_ptr)))
      {
	/* If we get here, we had a problem reading the file */
	fclose(fp);
	// FIXME: should copy cleanup calls from end of function here ?
	png_destroy_write_struct(&png_ptr, &info_ptr);
	return (-1);
      }

    /* Fill in image info */
    png_set_IHDR(png_ptr, info_ptr, imgp->width, imgp->height, 8,
		 PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
		 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    if (quick) {
      png_set_compression_level(png_ptr, 1);
      png_set_filter(png_ptr, PNG_FILTER_TYPE_BASE, PNG_FILTER_NONE);
    }

    /* Image data */
    png_bytep* row_pointers =
      (png_bytep*)png_malloc(png_ptr, imgp->height*png_sizeof(png_bytep));
    for (unsigned i=0; i<imgp->height; i++)
      row_pointers[i] = (png_bytep)imgp->ptr(0,i);
    png_set_rows(png_ptr, info_ptr, row_pointers);

    /* set up the output control if you are using standard C streams */
    png_init_io(png_ptr, fp);

    /* This is the easy way.  Use it if you already have all the
     * image info living info in the structure.  You could "|" many
     * PNG_TRANSFORM flags into the png_transforms integer here.
     */
    png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, png_voidp_NULL);

    /* clean up after the write, and free any memory allocated */
    png_free(png_ptr, row_pointers);
    png_destroy_info_struct(png_ptr, &info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);

    /* close the file */
    fclose(fp);

    /* that's it */
    return (0);
  }

}

/*
 * This is a test program for experimenting with edge detection along
 * a line.  Robust edge detection is a prerequisite to detection of
 * image features.
 *
 * TODO:
 * - abstract an Algorithm class and slim down find_edges() accordingly
 * - store and display threshold in AlgoResult
 * - overall mark for AlgoResult
 * - better 2-pixel edge detection (and n-pixel to allow blurred photos ?)
 * - separate operating modes:
 *  * seeding of data file using predefined lines
 *  * calibration checking using data file
 * - use refs to MatchData in false_matches list ?
 *   (not easy for filtered algorithms)
 */

#include "sImLib/Image.hh"

#include "log.hh"
#include "ImageInfo.hh"
#include "util.hh"

#include <exception>
#include <vector>

#include <cassert>
#include <cstdio>
#include <cstdarg>
#include <string>

using namespace goir;

//FIXME: this is ugly
namespace goir {
namespace log {
  unsigned flags;
}
}

int main(int argc, char* argv[])
{
  log::flags = log::bitmask(log::data_out,
			    log::wm_summary,
			    log::histo,
			    log::NULLFLAG);

  // image loading and safety checks
  assert(argc==2);
  std::string imgname(argv[1]);
  const sImLib::Image img(imgname);
  //assert(img.dimv() == 3);

  ImageInfo img_info(&img);
  img_info.maybe_read_edge_data();
  if (img_info.lines.size() == 0) {
    // generate standard lines
    const unsigned w = img.width, h = img.height;
    const unsigned cx = w/2, cy = h/2, rx=w/3, ry = h/3;
    img_info.add_line(cx, cy-ry, cx, cy+ry);
    img_info.add_line(cx-rx, cy, cx+rx, cy);
    img_info.add_line(cx-rx*7/10, cy-ry*7/10, cx+rx*7/10, cy+ry*7/10);
    img_info.add_line(cx-rx*7/10, cy+ry*7/10, cx+rx*7/10, cy-ry*7/10);
  }

  img_info.collect_histo_from_lines();
  if (log::is_set(log::histo))
    img_info.display_histo();

  // attempt (or check) edge detection
  img_info.look_for_edges();

  // evaluate accurateness of detection
  //img_info.evaluate();

  if (log::is_set(log::data_out))
    img_info.fprint(stdout);

  // results visualisation for selected algorithms
  sImLib::Image dpy_img(img);
  const_foreach(l, img_info.lines) {
    (*l)->results[goir::ImageInfo::ALGO_1PIX_COMPSUM_FILTERED].draw_on(dpy_img);
  }
  dpy_img.display();

  return 0;
}

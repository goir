#include "sImLib/img_path.hh"

#include "ImageInfo.hh"
#include "log.hh"
#include "util.hh"

#include <cerrno>
#include <climits>
#include <cmath>
#include <cstdlib>

namespace goir {

  // number of previous pixels kept in memory while scanning
  static const unsigned NKEPTPIXES = 2;
  struct prevpixes { // cannot be a STL container since pixel has to be a C array
    sImLib::pixel data[NKEPTPIXES];
    unsigned used;
  };

  void ImageInfo::AlgoResults::update_watermarks(bool is_known_edge,
						 unsigned value) {
    if (is_known_edge) { log::trace(log::wm_trace, "edge, ");
      if (value < min_positive) {
	log::trace(log::wm_trace, "smaller than %u - new record\n", min_positive);
	min_positive = value;
      } else
	log::trace(log::wm_trace, "larger than %u\n", min_positive);
    } else { log::trace(log::wm_trace, "no, ");
      if (value > max_negative) {
	log::trace(log::wm_trace, "larger than %u - new record\n", max_negative);
	max_negative = value;
      } else
	log::trace(log::wm_trace, "smaller than %u\n", max_negative);
    }
  }
  void ImageInfo::AlgoResults::fprint(FILE* outf) {
    std::vector<MatchData>::const_iterator m = missed.begin();

    // First precompute y drift direction to know how to order 2
    // points.
    // FIXME: be more clever about null drifts ?
    bool positive_y_drift =
      (found_edges[0].y <= found_edges[1].y);
    log::trace(log::debug_eval, "y drift is %s\n",
	       positive_y_drift ? "positive" : "negative");
    // Assume x drift is never negative
    assert(found_edges[0].x <= found_edges[1].x);

    const_foreach(f, found_edges) {
      // print all missed edges up to now
      while (m != missed.end() && ((m->x < f->x) ||
				   (positive_y_drift ? (m->y < f->y)
						     : (m->y > f->y)))) {
	fprintf(outf, "(%u,%u)(%u) ", m->x, m->y, m->delta);
	m++;
      }

      if (f->false_match)
	fprintf(outf, "*%u,%u(%u) ", f->x, f->y, f->delta);
      else
	fprintf(outf, "%u,%u(%u) ", f->x, f->y, f->delta);
    }

    // remaining missed if any
    for (; m != missed.end(); m++)
      fprintf(outf, "(%u,%u)(%u) ", m->x, m->y, m->delta);

    fprintf(outf, "\n");
  }

  void ImageInfo::AlgoResults::draw_on(sImLib::Image & img) {
    // light drawing of the scanline
    img.draw_line(linedata->x0, linedata->y0,
		  linedata->x1, linedata->y1,
		  sImLib::black, 0.2);

    // direction of drawing, orthogonal to line
    int vec_x = -(linedata->y1 - linedata->y0);
    int vec_y = (linedata->x1 - linedata->x0);
    int vec_divisor = hypot(vec_x, vec_y);

    std::vector<MatchData>::const_iterator m = missed.begin();

    const_foreach(f, found_edges) {
      // all missed edges up to now
      while (m != missed.end() && ((m->x < f->x) || (m->y < f->y))) {
	// FIXME: use missed delta when available
	img.draw_line(m->x, m->y,
		      m->x - (int)m->delta * vec_x / vec_divisor,
		      m->y - (int)m->delta * vec_y / vec_divisor,
		      sImLib::magenta, 0.5);
	m++;
      }

      sImLib::pixel const *color;
      if (f->false_match)
	color = &sImLib::cyan;
      else
	color = &sImLib::red;
      img.draw_line(f->x, f->y,
		    f->x + (int)f->delta * vec_x / vec_divisor,
		    f->y + (int)f->delta * vec_y / vec_divisor,
		    *color, 0.5);
    }

    // remaining missed if any
    for (; m != missed.end(); m++)
	// FIXME: use missed delta when available
	img.draw_line(m->x, m->y,
		      m->x - (int)m->delta * vec_x / vec_divisor,
		      m->y - (int)m->delta * vec_y / vec_divisor,
		      sImLib::magenta, 0.5);
  }

  // Read a list of unsigned values as x of edges along the line
  // FIXME: other code relies on increasing order, should assert that ?
  void ImageInfo::LineData::read_edges(FILE* f) {
    std::pair<unsigned,unsigned> values;
    while (fscanf(f, " %u,%u", &values.first, &values.second) == 2) {
      known_edges.push_back(values);
      // skip any parenthesized delta leftover from copypaste
      int c = getc(f);
      if (c == EOF)
	break;
      else if (c == '(') {
	// skip until matching ')'
	do {
	  c = getc(f);
	} while ((c != EOF) && (c != ')'));
      } else
	if (EOF == ungetc(c, f)) {
	  perror("ungetc"); exit(1);
	}
    }
    if (ferror(f)) {
      perror("fscanf"); exit(1);
    }
    _edges_are_known = true;
  }

  void ImageInfo::LineData::maybe_update_watermarks(int algo_id,
						    unsigned x,
						    bool is_known_edge,
						    unsigned value) {
    if (! edges_are_known())
      return;

    log::trace(log::wm_trace, "(%u) [%u] %u ? ", x, algo_id, value);

    results[algo_id].update_watermarks(is_known_edge, value);
  }

  void ImageInfo::LineData::find_edges() {
    // FIXME: these are dummy values - goal is deriving them from ImageInfo
    const unsigned percomp_threshold_1pix = 25;
    const unsigned summed_threshold_1pix = 50;
    const unsigned percomp_threshold_2pix = 35;
    const unsigned summed_threshold_2pix = 85;

    unsigned next_known_edge = 0;
    bool near_edge = false; // for filtering
    struct prevpixes prevpixes;
    prevpixes.used = 0;
    //const sImLib::pixel* colors[3] = { &red, &green, &blue };
    sImLib_forline(ii->imgp, ptrd,
		   x0, y0, x1, y1,

		   unsigned x=ii->imgp->x(ptrd);
		   unsigned y=ii->imgp->y(ptrd);
		   unsigned sum1pix=0;
		   unsigned maxdelta1=0;
		   unsigned sum2pix=0;
		   unsigned maxdelta2=0;
		   bool edge_found = false;

		   bool is_known_edge = (known_edges.size() > 0 &&
				   (x >= known_edges[next_known_edge].first) &&
				   (y >= known_edges[next_known_edge].second));
		   if (is_known_edge) next_known_edge++;

		   // no algo has anything to do on 1st point
		   if (prevpixes.used < 1)
		     goto endofloop;

		   unsigned delta1[3];
		   unsigned delta2[3];
		   for (int k = 0; k<3; k++) {
		     // algo: 1-pixel per-component (1/2)
		     delta1[k] = abs((*ptrd)[k] - (int)prevpixes.data[0][k]);
		     if (delta1[k]>maxdelta1) maxdelta1 = delta1[k];
		     // end algo
		     // algo: 1-pixel summed (1/2)
		     sum1pix += delta1[k];
		     // end algo

		     // following algos need 2 previous pixels recorded
		     if (prevpixes.used < 2)
		       continue;

		     // algo: 2-pixel per-component (1/2)
		     delta2[k] = abs((*ptrd)[k] - (int)prevpixes.data[1][k]);
		     if (delta2[k]>maxdelta2) maxdelta2 = delta2[k];
		     // end algo
		     // algo: 2-pixel summed (1/2)
		     sum2pix += delta2[k];
		     // end algo
		   }

		   // algo: 1-pixel per-component (2/2)
		   {
		     ImageInfo::AlgoResults::MatchData m(x,y,
							 abs(maxdelta1-percomp_threshold_1pix),
							 is_known_edge);
		     if (maxdelta1 >= percomp_threshold_1pix) {
		       results[ALGO_1PIX_PERCOMP].found_edges.push_back(m);
		       edge_found = true;
		     } else if (is_known_edge)
		       results[ALGO_1PIX_PERCOMP].missed.push_back(m);
		     maybe_update_watermarks(ALGO_1PIX_PERCOMP, x, is_known_edge, maxdelta1);
		   }
		   // end algo
		   // algo: 2-pixel per-component (2/2)
		   {
		     ImageInfo::AlgoResults::MatchData m(x,y,
							 abs(maxdelta2-percomp_threshold_2pix),
							 is_known_edge);
		     if (maxdelta2 >= percomp_threshold_2pix) {
		       results[ALGO_2PIX_PERCOMP].found_edges.push_back(m);
		       edge_found = true;
		     } else if (is_known_edge)
		       results[ALGO_2PIX_PERCOMP].missed.push_back(m);
		     maybe_update_watermarks(ALGO_2PIX_PERCOMP, x, is_known_edge, maxdelta2);
		   }
		   // end algo

		   // algo: 1-pixel summed (2/2) and filtered
		   {
		     ImageInfo::AlgoResults::MatchData m(x,y,
							 abs(sum1pix-summed_threshold_1pix),
							 is_known_edge);
		     if (sum1pix >= summed_threshold_1pix) {
		       results[ALGO_1PIX_COMPSUM].found_edges.push_back(m);

		       if (near_edge) {
			 ImageInfo::AlgoResults::MatchData & prev =
			   results[ALGO_1PIX_COMPSUM_FILTERED].found_edges.back();
			 if (prev.delta < m.delta) {
			   // overwrite previous match with better one
			   prev = m;
			 }
		       } else {
			 results[ALGO_1PIX_COMPSUM_FILTERED].found_edges.push_back(m);
			 near_edge = true;
		       }
		     } else {
		       near_edge = false;
		       if (is_known_edge) {
			 results[ALGO_1PIX_COMPSUM].missed.push_back(m);
			 results[ALGO_1PIX_COMPSUM_FILTERED].missed.push_back(m);
		       }
		     }
		     maybe_update_watermarks(ALGO_1PIX_COMPSUM, x, is_known_edge, sum1pix);
		     // FIXME: such watermaking inadequate with filtering
		   }
		   // end algo
		   // algo: 2-pixel summed (2/2)
		   {
		     ImageInfo::AlgoResults::MatchData m(x,y,
							 abs(sum2pix-summed_threshold_2pix),
							 is_known_edge);
		     if (prevpixes.used >= 2 &&
			 sum2pix >= summed_threshold_2pix) {
		       results[ALGO_2PIX_COMPSUM].found_edges.push_back(m);
		       edge_found = true;
		     } else if (is_known_edge)
		       results[ALGO_2PIX_COMPSUM].missed.push_back(m);
		     maybe_update_watermarks(ALGO_2PIX_COMPSUM, x, is_known_edge, sum2pix);
		   }
		   // end algo

		   //if (edge_found) goir::permute(colors[0],colors[1],colors[2]);

		   if (edge_found) {
		     log::trace(log::edge_details,
				"# %u: [%u,%u,%u] -> [%u,%u,%u] : "
				"%u = %u+%u+%u\n",
				x, color_components(prevpixes.data[0]),
				color_components((*ptrd)),
				sum1pix, color_components(delta1));
		   }

		   endofloop:
		   if (prevpixes.used < NKEPTPIXES)
		     prevpixes.used++;
		   else
		     memcpy(prevpixes.data+1, prevpixes.data,
			    sizeof(sImLib::pixel)*(NKEPTPIXES-1));
		   memcpy(prevpixes.data[0], ptrd, sizeof(sImLib::pixel));
		   //memcpy(ptrd, colors[0], sizeof(sImLib::pixel));
		   );

    if (edges_are_known()) {
      log::trace(log::wm_summary, "COMP1: - %u | %u -\n",
		 results[ALGO_1PIX_PERCOMP].max_negative,
		 results[ALGO_1PIX_PERCOMP].min_positive);
      log::trace(log::wm_summary, "GLOB1: - %u | %u -\n",
		 results[ALGO_1PIX_COMPSUM].max_negative,
		 results[ALGO_1PIX_COMPSUM].min_positive);
      log::trace(log::wm_summary, "COMP2: - %u | %u -\n",
		 results[ALGO_2PIX_PERCOMP].max_negative,
		 results[ALGO_2PIX_PERCOMP].min_positive);
      log::trace(log::wm_summary, "GLOB2: - %u | %u -\n",
		 results[ALGO_2PIX_COMPSUM].max_negative,
		 results[ALGO_2PIX_COMPSUM].min_positive);
    }
  }

  void ImageInfo::LineData::fprint(FILE* outf) {
    fprintf(outf, "[%u %u %u %u]\n", x0, y0, x1, y1);
    if (known_edges.size() > 0)
      known_edges.fprint(outf);
    for (unsigned i = 0; i < results.size(); i++) {
      fprintf(outf, "F%u:", i);
      results[i].fprint(outf);
    }
  }

  void ImageInfo::maybe_read_edge_data() {
    std::string datafilename(imgp->filename);
    datafilename += ".data";
    FILE* f = fopen(datafilename.c_str(), "r");
    if (f == NULL) {
      if (errno != ENOENT) {
	perror("fopen .data"); exit(1);
      }
      // else there is simply no .data file
    } else {
      LineData* line = NULL;
      do {
	int ret = getc(f);
	if (ret == EOF) break;
	if (ret == '[') { // line coords
	  unsigned x0,y0,x1,y1;
	  int ret = fscanf(f, "%u %u %u %u", &x0, &y0, &x1, &y1);
	  if (ret == EOF) {
	    fprintf(stderr, "EOF reading line coords\n"); exit(1);
	  } else if (ret < 4) {
	    fprintf(stderr, "Error: not enough numbers (%d) for line coords\n", ret); exit(1);
	  }
	  line = new LineData(this, x0, y0, x1, y1);
	} else if (isdigit(ret)) { // edges
	  if (!line) {
	    fprintf(stderr, "Error: edges data with no previous line coords\n"); exit(1);
	  }
	  if (ungetc(ret, f) == EOF) {
	    perror("ungetc"); exit(1);
	  }
	  line->read_edges(f);
	  {
	    // make sure that there is always a "next edge"
	    std::pair<unsigned,unsigned> pair;
	    pair.first = imgp->width+1;
	    pair.second = imgp->height+1;
	    line->known_edges.push_back(pair);
	  }
	  lines.push_back(line);
	  line = NULL;
	} else {
	  // consume till EOL
	  char* lineptr = NULL;
	  size_t dummy;
	  errno = 0;
	  getline(&lineptr, &dummy, f);
	  if (ferror(f)) {
	    perror("getline"); exit(1);
	  }
	  if (lineptr) free(lineptr);
	}
      } while(!feof(f));
      fclose(f);
    }
  }

  void ImageInfo::add_to_histo(unsigned x0, unsigned y0, unsigned x1, unsigned y1,
			       UVector *known_edges) {
    // walk over the line for histo data
    unsigned next_known_edge = 0;
    struct prevpixes prevpixes;
    prevpixes.used = 0;
    sImLib_forline(imgp, ptrd,
		   x0, y0, x1, y1,

		   // FIXME: check what happens if we start from other end of line

		   unsigned x=imgp->x(ptrd);
		   unsigned y=imgp->y(ptrd);
		   unsigned sum1pix=0;
		   unsigned maxdelta1=0;
		   unsigned histo_selector = NONEDGES;

		   // no algo has anything to do on 1st point
		   if (prevpixes.used < 1)
		     goto endofloop;

		   // in which set of histograms shall we record this point ?
		   if (known_edges->size() > 0) {
		     if ((x >= (*known_edges)[next_known_edge].first) &&
			 (y >= (*known_edges)[next_known_edge].second)) {
		       next_known_edge++;
		       histo_selector = EDGES;
		     }
		     if ((x > (*known_edges)[next_known_edge].first) ||
			 (y > (*known_edges)[next_known_edge].second))
		       fprintf(stderr, "Missed an edge, using approximation!\n");
		   }

		   for (int k = 0; k<3; k++) {
		     unsigned delta1 = abs((*ptrd)[k] - (int)prevpixes.data[0][k]);
		     if (delta1>maxdelta1) maxdelta1 = delta1;
		     histos_c[histo_selector][k].record(delta1);
		     sum1pix += delta1;
		   }
		   histos[histo_selector].record(sum1pix);
		   histos_allc[histo_selector].record(maxdelta1);

		   endofloop:
		   if (prevpixes.used < NKEPTPIXES)
		     prevpixes.used++;
		   else
		     memcpy(prevpixes.data+1, prevpixes.data,
			    sizeof(sImLib::pixel)*(NKEPTPIXES-1));
		   memcpy(prevpixes.data[0], ptrd, sizeof(sImLib::pixel));

		   );
  }

  void ImageInfo::display_histo() {
    printf("N: "), histos[NONEDGES].display();
    if (histos[NONEDGES].samples())
      printf("FP: "), histos[NONEDGES].display_potential_false_matches();
    if (histos[EDGES].samples())
      printf("E: "), histos[EDGES].display();

    for (int k = 0; k<3; k++) {
      printf("C%d N: ", k), histos_c[NONEDGES][k].display();
      if (histos_c[NONEDGES][k].samples())
	printf("C%d FP: ", k), histos_c[NONEDGES][k].display_potential_false_matches();
      if (histos_c[EDGES][k].samples())
	printf("C%d E: ", k), histos_c[EDGES][k].display();
    }
    printf("C_ N: "), histos_allc[NONEDGES].display();
    if (histos_allc[NONEDGES].samples())
      printf("C_ FP: "), histos_allc[NONEDGES].display_potential_false_matches();
    if (histos_allc[EDGES].samples())
      printf("C_ E: "), histos_allc[EDGES].display();
  }

}

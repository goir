#ifndef _GOIR_UVECTOR_HH
#define _GOIR_UVECTOR_HH

#include <utility>
#include <vector>

namespace goir {

  class UVector: public std::vector< std::pair<unsigned,unsigned> > {
  public:
    void fprint(FILE* outf) {
      for (std::vector< std::pair<unsigned,unsigned> >::const_iterator i = begin();
	   i != end();
	   i++)
	fprintf(outf, "%u,%u ", i->first, i->second);
      fprintf(outf, "\n");
    }
  };

}

#endif // _GOIR_UVECTOR_HH

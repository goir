#ifndef _GOIR_UTIL_HH
#define _GOIR_UTIL_HH

namespace goir {
  template<typename T> inline void permute(T& a,T& b, T& c) { T t=a; a=b; b=c; c=t; }
}

#define color_components(c) (c)[0],(c)[1],(c)[2]

// "foreach" macros, more useful to me than BOOST_FOREACH:

namespace goir {
  template<typename T>
  struct const_iter
  { typedef typename T::const_iterator type; };

  template<typename T>
  struct iter
  { typedef typename T::iterator type; };
}

#define const_foreach(i,container)					\
  for (goir::const_iter<typeof(container)>::type i = container.begin();	\
       i != container.end(); i++)

#define foreach(i,container)						\
  for (goir::iter<typeof(container)>::type i = container.begin();	\
       i != container.end(); i++)

#endif // _GOIR_UTIL_HH
